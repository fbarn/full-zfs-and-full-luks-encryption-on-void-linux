#!/bin/bash

# message to user
echo "This script unmounts the bind mounts and exports the pool, before rebooting the machine."

for i in sys proc dev/pts dev efi
do
  echo -n "unmounting $i..."
  umount -R ${TARGET}/$i
  echo ' completed.'
done

/sbin/zfs unmount -a

/sbin/zpool export -a -f

# final message to user
echo "When you reboot, sign in as *root* and execute in root's home directory:"
echo ". 08-post-installation-setup.sh"
echo ""
read -p "Press <return> to reboot." rebootnow
reboot
