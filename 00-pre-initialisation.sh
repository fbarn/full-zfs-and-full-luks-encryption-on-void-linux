#!/bin/bash

# assuming use of hrmpf [ https://github.com/leahneukirchen/hrmpf/ ] or Ubuntu livecd
# - none of this is perhaps strictly necessary and may change

# TODO: add system-detection

USERNAME=`whoami`

if [ ${USERNAME} == "root" ]
then
    echo "This script sets up the needed tools for preparing a ZFS LUKS encrypted partition."
    DISTROUSED=`lsb_release -si`

    if [ ${DISTROUSED} == "VoidLinux" ]
    then
	# get rid of old kernel to save dkms compilation time
	xbps-remove linux4.9 linux4.9-headers 

	# update essential things - xbps
	xbps-install -Su

	# warn user about time
	echo "You may want to go and put the kettle on; this can take a bit"

	# then install up-to-date zfs (the long bit)
	xbps-install zfs

	# modprobe: activate zfs
	/sbin/modprobe zfs

	# message to user
	echo "You have installed the needed tools on the chroot-host machine."
	echo "You are now ready to proceed to the next step; execute the following in the terminal:"
	echo ". 01-cryptsetup.sh"

    elif [ ${DISTROUSED} == "Ubuntu" ]
    then

	# update repos
	apt update

	# install zfs components
	apt install zfsutils-linux
         
	# modprobe: activate zfs
	/sbin/modprobe zfs

	# message to user
	echo "You have installed the needed tools on the chroot-host machine."
	echo "You are now ready to proceed to the next step; execute the following in the terminal:"
	echo ". 01-cryptsetup.sh"

    else
	echo "I'm not sure what system you're using. You probably know. Do what you need to and move to the next script."
    fi

else
    echo "You need to be root. Please become root and try again."
fi
