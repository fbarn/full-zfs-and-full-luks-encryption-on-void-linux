#!/bin/bash

. importvars.sh # get old variables from previous steps too

if [ ${NETWORKMAN} == "Y" ]
then
    rm -fr /var/service/dhcpcd
    rm -fr /var/service/wpa_supplicant
    ln -s /etc/sv/NetworkManager /var/service
    ln -s /etc/sv/dbus /var/service
    # inform user how to enable network
    echo "Once this script finishes, you can connect to a WiFi network immediately (without going through a full GUI/desktop environment), by running `nmtui` (NetworkManager's text user interface) and activating/signing into the appropriate network."
fi

# messages to user
echo "You will want to continue post-installation configuration of Void, perhaps particularly including network configuration and enabling of sudo access for wheel group via 'visudo'."
echo "Please refer to the Void Wiki, particularly:"
echo "https://wiki.voidlinux.org/Network_Configuration"
echo "https://wiki.voidlinux.org/Post_Installation"
echo "(Some things are being migrated to https://docs.voidlinux.org , which you might also refer to.)"
echo ""
read -p "Press <return> to clean-up files and exit from root." exiting
rm importvars.sh
rm 07-post-installation-setup.sh
exit
