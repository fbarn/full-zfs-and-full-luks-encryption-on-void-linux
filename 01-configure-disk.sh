#!/bin/bash

echo "Use this script with care. Whatever device you choose to install to will be completely formatted and any data on it will be lost."

echo "Once this is done, two new partitions will be created: a 512M EFI partition, and a Solaris Root partition which will occupy the remainder of the space."

echo "Here and throughout, hitting the <return> key accepts the default value shown in [brackets]."

read -p "Enter the device you want to install to [/dev/sda]: " _DEVICE
_DEVICE=${_DEVICE:-/dev/sda}
DEVICE=/dev/disk/by-id/$(ls -la /dev/disk/by-id/ | grep $(echo ${_DEVICE} | cut -f3 -d"/") -m1 | awk '{print $9}')

# determine if device is SSD or not
ROTATIONAL=`cat /sys/block/"${_DEVICE#/dev/}"/queue/rotational`

read -p "You want to install to ${_DEVICE}? Are you sure? Proceeding will irrevocably wipe ${DEVICE}. Please type 'yes' in ALLCAPS to proceed: " proceed

if [ ${proceed} == "YES" ]
then
    read -p "How much swap space do you want? [8G]: " SWAPSIZE
    SWAPSIZE=${SWAPSIZE:-8G}
    # erase drive(!)
    sgdisk --zap-all ${DEVICE}

    # Set MBR
    sgdisk     -n1:1M:+512M   -t1:EF00 ${DEVICE}
    sgdisk     -n2:0:${SWAPSIZE}      -t2:8200 ${DEVICE}
    sgdisk     -n3:0:0        -t3:BF00 ${DEVICE}
    mkfs.vfat ${DEVICE}-part1

    # message to user
    echo "You have now prepared ${DEVICE}1 Solaris Root partition. You may proceed to the next step; execute the following in the terminal:"
    echo ". 02-zpool-creation.sh"
else
    echo "Cancelled."
fi
