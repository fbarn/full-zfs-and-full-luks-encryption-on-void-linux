#!/bin/bash

# message to user
echo "This script, executed inside of the chroot, does the bulk of the basic configuration of Void Linux. Since ZFS DKMS module needs to be built against the kernel, this step will take the longest, and may represent a good time to prepare a cup of tea/coffee and a snack. There will be a large-ish amount of prepatory questions about your timezone, language, keyboard layout, set root and user password, and so on."
mv /etc/os-release /usr/lib/os-release
ln -rs /usr/lib/os-release /etc
# timezone
echo "Please enter one of the following time regions:"
ls -l /usr/share/zoneinfo | awk '{print $9}' | tr '\n' ' '
read -p "Region: " TIMEREGION

if [[ `ls /usr/share/zoneinfo/${TIMEREGION}` ]]
then
    echo "Please enter one of the following time zones/representative cities:"
    ls -l /usr/share/zoneinfo/${TIMEREGION} | awk '{print $9}' | tr '\n' ' '
    read -p "Timezone: " TIMEZONE
else
    echo "That is not a valid region. Choosing UTC."
    TIMEREGION=UTC
fi

#read -p "Please enter your desired locale/lang [en_US.UTF-8]: " LOCALECHOICE
#LOCALECHOICE=${LOCALECHOICE:-"en_US.UTF-8"}

read -p "Please enter your desired keyboard layout [us] :" KEYBOARDLAY
KEYBOARDLAY=${KEYBOARDLAY:-us}

read -p "Please enter your desired system name ('host name') [myZFSVoid]: " SYSTEMNAME
SYSTEMNAME=${SYSTEMNAME:-myZFSVoid}

. importvars.sh # get old variables from previous steps too

#echo "LANG=${LOCALECHOICE}" > /etc/locale.conf
#echo "LC_COLLATE=C" >> /etc/locale.conf

#if [ ${LOCALECHOICE} == "en_US.UTF-8" ]
#then
    # echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales # already enabled
#    echo "Setting locales..."
#else
    # echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales already enabled
#    echo "Setting locales..."
#    echo "${LOCALECHOICE} UTF-8" >> /etc/default/libc-locales
#fi
#xbps-reconfigure -f glibc-locales

echo ${SYSTEMNAME} > /etc/hostname
echo "HOSTNAME=\"${SYSTEMNAME}\"" >> /etc/rc.conf
echo "HARDWARECLOCK=\"UTC\"" >> /etc/rc.conf
if [ ${TIMEREGION} == "UTC" ]
then
    echo "TIMEZONE=\"UTC\"" >> /etc/rc.conf
else
    echo "TIMEZONE=\"${TIMEREGION}/${TIMEZONE}\"" >> /etc/rc.conf
fi
echo "KEYMAP=\"${KEYBOARDLAY}\"" >> /etc/rc.conf

echo "Set new root password below "
until passwd; do echo "Confirmation didn't match. Please try again. " ; done

# install necessary packages
xbps-install -S xbps libxbps # update xbps

read -p "The following will take some time - you may want to go and put the kettle on or the like. Please press <return> (twice) to continue." teatime

# install crucial packages, including zfs, which will trigger DKMS ZFS module creation for each kernel
xbps-install -S linux zfs efibootmgr gummiboot sbsigntool

mv 30-bootentry /etc/kernel.d/post-install
#mv 40-sbsigntool /etc/kernel.d/post-install
mkdir /etc/efikeys
mv mkkeys.sh /etc/efikeys
#cd /etc/efikeys
cat efibootmgr-kernel-hook > /etc/default/efibootmgr-kernel-hook
#. mkkeys.sh

# disable resume functionality from dracut
echo "omit_dracutmodules+=\" btrfs resume \"" > /etc/dracut.conf.d/omit-resume-for-zvol-swap.conf
echo "add_dracutmodules+=\" zfs \"" > /etc/dracut.conf.d/zvol.conf
echo "hostonly=\" yes\"" >> /etc/dracut.conf.d/zvol.conf
echo "nofsck=\" yes\"" >> /etc/dracut.conf.d/zvol.conf

# inform user about resume
echo "[Resume functionality has been disabled from dracut. Otherwise future kernels which are installed will take 2-3 minutes to boot while dracut searches for the swap partition to initialise resume. This issue will be investigated, but disabling resume from dracut prevents the delayed boot.]"

# dracut
sed -i 's/--force/--force --hostonly --include \/boot\/rootkey.bin \/rootkey.bin/' /etc/kernel.d/post-install/20-dracut

TRUELINUXVERSION=`xbps-query -x linux | head -n1 | cut -f1 -d">"`
echo PARTUUID=$(blkid -s PARTUUID -o value ${DEVICE}-part1) \
    /efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro 0 2 >> /etc/fstab

#read -p "How much swap space do you want? [8G]: " SWAPSIZE
#SWAPSIZE=${SWAPSIZE:-8G}

# set up swap
#echo "Setting up swap and updating fstab..."
#zfs create -o sync=always -o primarycache=metadata -o secondarycache=none -b 4k -V ${SWAPSIZE} -o logbias=throughput ${ZPOOLNAME}/swap
#mkswap -f /dev/zvol/${ZPOOLNAME}/swap
#echo "# zol swap vol" >> /etc/fstab
#echo "/dev/zvol/${ZPOOLNAME}/swap	none	swap	sw		0	0" >> /etc/fstab

# Setting zfs cache
echo "Setting zpool cachefile for ${ZPOOLNAME}."
zpool set cachefile=/etc/zfs/zpool.cache ${ZPOOLNAME} # IMPORTANT

# setting autotrim feature
if [[ ${ROTATIONAL} == 0 ]]
then
    read -p "You appear to be running an SSD. You can set up automatic TRIM for better wear-levelling performance. (You can always turn this feature on/off later, or handle TRIM manually.)

 Note that minimal data leakage in the form of freed block information, perhaps sufficient to determine the filesystem in use, may occur on LUKS-encrypted devices with TRIM enabled (see https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#Discard/TRIM_support_for_solid_state_drives_(SSD) for more information).

Would you like to set up automatic TRIM? [Y/n]: " TRIM
    TRIM=${TRIM:-Y}
    if [ ${TRIM} == "Y" ] || [ ${TRIM} == "y" ]
    then
        zpool upgrade ${ZPOOLNAME}
        sudo zpool set autotrim=on ${ZPOOLNAME}
        echo "Autotrim feature enabled on ${ZPOOLNAME}."
    else
        echo "Autotrim NOT enabled."
    fi
fi

# setting ARC
read -p "Would you like to set limit ARC size? (By default it will default to half available memory; if you're running a desktop/laptop, you may want to set it lower; if you're running a file-server, you may want to set it higher.) [Y/n]: "  ARCSET
ARCSET=${ARCSET:-Y}

if [ ${ARCSET} == "Y" ] || [ ${ARCSET} == "y" ]
then
    RAWMEM=`cat /proc/meminfo | grep MemTotal | awk '{print $2}'`
    #   MEMAVAIL=`echo $(((${RAWMEM}*1.025)/1024)/1024)`
    MEMAVAIL=`awk -v RAWMEM=$RAWMEM 'BEGIN {printf "%.0f\n", (((RAWMEM * 1.025)/1024) / 1024) }'`

    echo "You have ${MEMAVAIL}Gb available. How much memory would you like to reserve for ARC? "
    until [[ "${ARCREZ}" =~ ^[0-9]+(\.[0-9]+)?$ ]] && [[ $largerthanzero == 1 ]] && [[ $biggerthanram == 1 ]]
    do read -p "Enter a number in Gb that is larger than 0 and less than ${MEMAVAIL}, (e.g. \"4\", \"1.76\"): " ARCREZ
       largerthanzero=`awk -v ARCREZ="$ARCREZ" 'BEGIN { print (ARCREZ > 0) }'`
       biggerthanram=`awk -v ARCREZ="$ARCREZ" -v MEMAVAIL="$MEMAVAIL" 'BEGIN { print (ARCREZ<MEMAVAIL) }'`
    done
    ARCREZINT=${ARCREZ%.*}
    MEMINBYTES=`echo "$((ARCREZINT*1024*1024*1024))"`
    #    echo "
    # Limit ZFS ARC to ${ARCREZ}Gb
    # echo ${MEMINBYTES} >> /sys/module/zfs/parameters/zfs_arc_max" >> /etc/rc.local # doesn't seem to work anymore (?!) from rc.local
    # so put into grub instead:
    sed -i "s/zfs.zfs_arc_max=/zfs.zfs_arc_max=$MEMINBYTES/" /etc/default/efibootmgr-kernel-hook
fi

# efibootmgr
sed -i "s@root=ZFS=@root=ZFS=${ZFSROOTDATASET}@" /etc/default/efibootmgr-kernel-hook
sed -i "s@DISK=@DISK=\"${_DEVICE}\"@" /etc/default/efibootmgr-kernel-hook
xbps-reconfigure -fa

# setting up regular user
useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input ${USERNAME}
echo "Set password for user ${USERNAME}: "
until passwd ${USERNAME}; do echo "Confirmation didn't match. Please try again. " ; done

# additional configuration
read -p "Do you want to install NetworkManager? [Y/n]: " NETWORKMAN
NETWORKMAN=${NETWORKMAN:-Y}

# networkmanager configuration
if [ ${NETWORKMAN} == "Y" ] ||  [ ${NETWORKMAN} == "y" ]
then
    echo "polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("network")) {
    return polkit.Result.YES;
  }
});" > /etc/polkit-1/rules.d/50-org.freedesktop.NetworkManager.rules
    xbps-install NetworkManager
    gpasswd -a ${USERNAME} network
fi

# export information for post-installation set-up
echo "export DEVICE=${DEVICE}" > /root/importvars.sh
echo "export ZPOOLNAME=${ZPOOLNAME}" >> /root/importvars.sh
echo "export USERNAME=${USERNAME}" >> /root/importvars.sh
echo "export NETWORKMAN=${NETWORKMAN}" >> /root/importvars.sh
echo "export ZFSROOTDATASET=${ZFSROOTDATASET}" >> ${TARGET}/importvars.sh
mv 07-post-installation-setup.sh /root/07-post-installation-setup.sh

# message to user
echo "You have successfully configured Void Linux. The next step is leaving the chroot, unmounting bind mounts and exporting the ZFS pool. These actions will be performed by the next script. After exiting the chroot, please execute the following in the terminal:"
echo ". 07-umount-reboot.sh"
read -p "Press <return> to exit chroot." exitchroot

# get rid of scripts
rm importvars.sh
rm 05-config-inside-chroot.sh

exit
