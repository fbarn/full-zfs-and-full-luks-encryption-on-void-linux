#!/bin/bash

# message to user
echo "This script sets up the need bind mounts for the chroot, and copies over necessary files (including a script to import relevant variables.)"

for i in /dev /dev/pts /proc /sys
do
  echo -n "mounting $i..."
  mount --rbind $i ${TARGET}$i
  echo ' completed.'
done

mkdir /mnt/efi
mount ${DEVICE}-part1 /mnt/efi

cp -p /etc/resolv.conf ${TARGET}/etc/ # copy DNS resolver configuration
cp /etc/hostid ${TARGET}/etc/ # copy hostid onto actual system

# copy cryptuuid & luksname into chroot location
echo "export DEVICE=${DEVICE}" > ${TARGET}/importvars.sh
echo "export _DEVICE=${_DEVICE}" >> ${TARGET}/importvars.sh
echo "export ZPOOLNAME=${ZPOOLNAME}" >> ${TARGET}/importvars.sh
echo "export USERNAME=${USERNAME}" >> ${TARGET}/importvars.sh
echo "export ROTATIONAL=${ROTATIONAL}" >> ${TARGET}/importvars.sh
echo "export ZFSROOTDATASET=${ZFSROOTDATASET}" >> ${TARGET}/importvars.sh
cp 05-config-inside-chroot.sh ${TARGET}
cp 07-post-installation-setup.sh ${TARGET}
cp 30-bootentry ${TARGET}
cp 40-sbsigntool ${TARGET}
cp efibootmgr-kernel-hook ${TARGET}
cp mkkeys.sh ${TARGET}

echo "After you enter <return> you will be in the Void chroot mounted on ${TARGET}. You should execute the next script in the chroot; entering in the terminal:"
echo ". 06-config-inside-chroot.sh"

read -p "Please press <return> to continue." enterchroot
chroot ${TARGET}

