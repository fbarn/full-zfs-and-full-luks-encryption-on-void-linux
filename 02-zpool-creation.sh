#!/bin/bash

# message to user
echo "This script creates an encrypted ZFS pool on the Solaris Root partition, mounting it locally/temporally in a directory of your choosing (by default, /mnt). You can choose a name for your ZFS pool and a username for the main user. As is the case for the other scripts, you may wish to edit the script by hand if you desire, for instance, a different ZFS data-set configuration."

# set hostid to head off issue of zfs thinking the pool is being imported by the wrong system

# use MAC address of first ethernet device (stripping out colons)
ip link sh | grep ether | awk '{print $2}' | tr -dc a-f0-9 > /etc/hostid

# or could just generate randomly, as below
# head /dev/urandom | tr -dc a-f0-9 | head -c 13 > /etc/hostid

read -p "Enter your planned (non-root) user-name. (You probably don't want the accept the default.) [flynn]: " USERNAME
USERNAME=${USERNAME:-flynn}

read -p "Where should the installation drive be temporarily mounted? (The default should be fine here.) [/mnt]: " TARGET
TARGET=${TARGET:-/mnt}

read -p "What name do you want to assign to the zpool? [tank] ('dozer' is another fine choice, as is 'cas', 'trinity', or 'morpheus'.): " ZPOOLNAME
ZPOOLNAME=${ZPOOLNAME:-tank}

ZFSROOTBASENAME=${ZPOOLNAME}/ROOT
ZFSROOTDATASET=${ZFSROOTBASENAME}/system

# you may want to adjust some of the below settings
 # 'compression=on' defaults currently to lz4, which is probably quicker than no encryption
 # 'ashift=12' is best practice, probably, at this point regardless of actual device
/sbin/zpool create -f \
            -R ${TARGET} \
            -O mountpoint=none \
	    -O acltype=posixacl \
            -O relatime=on \
            -O xattr=sa \
            -O compression=lz4 \
            -O normalization=formD \
            -o ashift=12 \
	    -O encryption=aes-256-gcm -O keylocation=prompt -O keyformat=passphrase \
            ${ZPOOLNAME} ${DEVICE}-part3

# adjust below datasets as desired - could be more elaborate, cp. https://github.com/zfsonlinux/zfs/wiki/Debian-Buster-Root-on-ZFS

# also this subvolume'd sort of system won't work for Bedrock Linux [ https://bedrocklinux.org/ ] , the entire rootfs (and probably /boot ) will need to be part of the same dataset

# root for essential system bits, perhaps to be snapshotted/cloned
/sbin/zfs create -o canmount=off                        ${ZFSROOTBASENAME}
/sbin/zfs create -o mountpoint=/                        ${ZFSROOTDATASET}
# boot dir (also perhaps to be snapshotted/cloned)
/sbin/zfs create -o mountpoint=/boot                    ${ZPOOLNAME}/boot
# home dirs
/sbin/zfs create -o mountpoint=/home                    ${ZPOOLNAME}/home
/sbin/zfs create -o mountpoint=/home/${USERNAME}        ${ZPOOLNAME}/home/${USERNAME}
/sbin/zfs create -o mountpoint=/root                    ${ZPOOLNAME}/home/root
# for things that we probably don't need to clone
/sbin/zfs create -o canmount=off                        ${ZPOOLNAME}/VAR
/sbin/zfs create -o mountpoint=/var                     ${ZPOOLNAME}/VAR/var
/sbin/zfs create -o mountpoint=/var/log                 ${ZPOOLNAME}/VAR/logs
/sbin/zfs create -o mountpoint=/var/cache/xbps          ${ZPOOLNAME}/VAR/distfiles
/sbin/zfs create -o mountpoint=/usr/local               ${ZPOOLNAME}/VAR/local
/sbin/zfs create -o mountpoint=/usr/src                 ${ZPOOLNAME}/VAR/src

# sets dataset to boot from:
/sbin/zpool set bootfs=${ZFSROOTDATASET} ${ZPOOLNAME} # Do not skip this step

# show the user what has been done:
/sbin/zpool status -v # print zpool info
/sbin/zpool list # print zpool list
/sbin/zfs list # show zfs filesystems

# message to user
echo "You have successfully created an encrypted ZFS pool ${ZPOOLNAME} and various ZFS datasets (see details immediately above). You may now proceed to the next step; execute the following in the terminal:"
echo ". 03-fetch-void-rootfs.sh"
