#!/bin/bash

# message to user
echo "This script uses xbps -r to install the Void Linux musl libc base-system into your ZFS/LUKS partition currently mounted on ${TARGET}. You may want to examine the different possible mirrors available at https://alpha.de.repo.voidlinux.org to choose your desired base, e.g. if you're installing on 32bit or ARM or want to use glibc, etc."

read -p "Which Void mirror to use [http://alpha.de.repo.voidlinux.org/live/current]: " VOIDMIRROR
VOIDMIRROR=${VOIDMIRROR:-https://alpha.de.repo.voidlinux.org/current/musl}

XBPS_ARCH=x86_64-musl xbps-install -S -R ${VOIDMIRROR} -r ${TARGET} base-system
